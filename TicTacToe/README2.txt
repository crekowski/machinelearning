Our player uses the following learning strategy:
- knowledge is represented as a linear function with weights
- boards are rated by extracting some features and evaluating the function on them
- possible future moves are tested for their quality and the best one is returned
- also some enemy moves (after the own future move) are regarded
- if a potential final state in the game is reached, the weights of the function are adjusted according to the LMS algorithm
- earlier boards are then also used for learning with linear decreasing target values

To test the learning phase of our player, just start a tournament with NewPlayer.
Also a version with fixed weights that were learned before is available.
To test this, start a tournament with FixedTrainedPlayer.

a)
The development of the performance of our system can be seen in the two diagrams diagram_random and diagram_smart.
Against the random player our algorithm wins all matches beginning with the first.
Against the smart player it loses often in the beginning, but is able to improve and reach draws.

b)
Our conclusions are the following:
- it is best, to train the algorithm against different opponents
-> against random to learn winning
-> against smart to learn stopping the opponent
- it is very difficult to exactly find the moves, that lead to the win
- yet simple strategies can be learned very fast (within one game!)
- the more moves the program plans ahead, the better it should get, but this is not feasible on bigger boards
-> our solution is not the best possible, but already achieves some good results
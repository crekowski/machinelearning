import java.util.List;
import java.util.PriorityQueue;

import de.ovgu.dke.teaching.ml.tictactoe.api.IBoard;
import de.ovgu.dke.teaching.ml.tictactoe.api.IMove;
import de.ovgu.dke.teaching.ml.tictactoe.api.IPlayer;
import de.ovgu.dke.teaching.ml.tictactoe.api.IllegalMoveException;
import de.ovgu.dke.teaching.ml.tictactoe.game.Move;

/**
 * This player uses the following learning strategy:
 * <ul>
 * <li> knowledge is represented as a linear function with weights
 * <li> boards are rated by extracting some features and evaluating the
 *      function on them
 * <li> possible future moves are tested for their quality and the best
 *      one is returned
 * <li> also some enemy moves (after the own future move) are regarded
 * <li> if a potential final state in the game is reached, the weights
 *      of the function are adjusted according to the LMS algorithm
 * <li> earlier boards are then also used for learning with linear decreasing
 *      target values
 * </ul>
 * 
 * @author Marianne Stecklina, Cornelius Styp von Rekowski
 */
public class NewPlayer implements IPlayer {

	public static final int NUM_OPP_MOVES = 5;
	
	protected Function function;
	
	/**
	 * create a new player without any learned weights
	 */
	public NewPlayer() {
		super();
		
		this.function = new Function(this, null);			
	}
	
	/**
	 * @return if the player should try to improve himself
	 */
	public boolean shouldLearn() {
		return true;
	}
	
	public String getName() {
		return "----CAPSLOCK----";
	}
	
	/**
	 * compares board values for all possible moves and returns best move
	 * 
	 * @param board current board
	 * @return best move
	 */
	public int[] makeMove(IBoard board) {
		IBoard                   copy;
		int[]                    field;
		IMove                    move;
		IMove                    bestMove;
		double                   boardValue;
		double                   bestValue;
		PriorityQueue<RatedMove> moveQueue;
		FeatureExtractor         extractor;
		Features                 currentFeatures;
		Features                 diffFeatures;
		Features                 newFeatures;
		
		bestMove        = null;
		bestValue       = Double.NEGATIVE_INFINITY;
		moveQueue       = new PriorityQueue<RatedMove>(125);
		extractor       = new FeatureExtractor(this);
		currentFeatures = extractor.getFeatures(board);
		
		// check all possible moves
		for (int x = 0; x < board.getSize(); x++) {
			for (int y = 0; y < board.getSize(); y++) {
				for (int z = 0; z < board.getSize(); z++) {
					field = new int[]{x, y, z};
					
					if (board.getFieldValue(field) == null) {
						// field is empty -> move is possible
						move = new Move(this, field);
						
						// make move
						copy = board.clone();
						try {
							copy.makeMove(move);
						} catch (IllegalMoveException e) {
							// should not happen
							e.printStackTrace();
						}
						
						// winning move
						if (copy.isFinalState()) {
							if (this.shouldLearn()) {
								function.learn(copy);
							}
							return move.getPosition();
						}
						
						// search for best board values
						diffFeatures = extractor.incrementFeatures(board, move);
						newFeatures  = currentFeatures.add(diffFeatures);
						boardValue   = this.function.evaluate(newFeatures); 
						
						moveQueue.add(new RatedMove(move, boardValue));
					}
				}
			}
		}
		
		// check oppononent's reaction for k best moves
		for (int i = 0; i < NUM_OPP_MOVES; i++) {
			move = moveQueue.poll().move;
			copy = board.clone();
			try {
				copy.makeMove(move);
			} catch (IllegalMoveException e) {
				continue;
			}
			
			// save best opponent's value
			boardValue = this.getBestOppMoveValue(copy);						
			if (boardValue > bestValue) {
				bestValue = boardValue;
				bestMove  = move;
			}				
		}		
		
		return bestMove.getPosition();
	}
	
	
	/**
	 * compares all possible opponent's moves and returns the board value for 
	 * opponent's best move
	 * 
	 * @param board board after own move
	 * @return board value for opponent's best move
	 */
	public double getBestOppMoveValue(IBoard board) {
		IBoard           copy;
		FeatureExtractor extractor;
		Features         currentFeatures;
		Features         diffFeatures;
		Features         newFeatures;
		int[]            field;
		IMove            move;
		double           worstValue;
		double           boardValue;
		IPlayer          opponent;
		
		worstValue      = Double.MAX_VALUE;
		extractor       = new FeatureExtractor(this);
		currentFeatures = extractor.getFeatures(board);
		
		// calculate features for all possible moves
		for (int x = 0; x < board.getSize(); x++) {
			for (int y = 0; y < board.getSize(); y++) {
				for (int z = 0; z < board.getSize(); z++) {
					field = new int[]{x, y, z};
					if (board.getFieldValue(field) == null) {
						// field is empty -> move is possible
						
						// get opponent from history
						List<IMove> history = board.getMoveHistory();
						if (history.size() < 2) {
							// opponent has not yet made a move -> simulate with NewPlayer
							opponent = new NewPlayer();
						}
						else {
							opponent = history.get(history.size() - 2).getPlayer();
						}
						
						move = new Move(opponent, field);
						
						diffFeatures = extractor.incrementFeatures(board, move);
						newFeatures  = currentFeatures.add(diffFeatures);
						boardValue   = this.function.evaluate(newFeatures);
						
						if (boardValue < worstValue) {
							worstValue = boardValue;
						}
						
						if (this.shouldLearn()) {
							// check for final state
							copy = board.clone();
							try {
								copy.makeMove(move);
							} catch (IllegalMoveException e) {
								// should not happen..
								e.printStackTrace();
							}				
							if (copy.isFinalState()) {
								// learn from negative example
								function.learn(copy);
							}
						}
					}
				}
			}
		}
		
		return worstValue;
	}
	
	public void onMatchEnds(IBoard board) {
		System.out.println("Result: " + function.toString());
	}

}

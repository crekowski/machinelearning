import de.ovgu.dke.teaching.ml.tictactoe.api.IBoard;
import de.ovgu.dke.teaching.ml.tictactoe.api.IMove;
import de.ovgu.dke.teaching.ml.tictactoe.api.IllegalMoveException;

public class Function {
	
	private static final double eta = 0.01;
	
	private double[]  weights;
	private NewPlayer player;
	
	/**
	 * Initialize the linear function with the given weights. If no weights are
	 * given, all weights are initialized with 1.
	 * 
	 * @param player
	 * @param learnedWeights
	 */
	public Function(NewPlayer player, double[] learnedWeights) {
		this.player = player;
		
		if (learnedWeights == null) {
			this.weights = new double[6];			
			for (int i = 0; i < this.weights.length; i++) {
				this.weights[i] = 1;
			}
		}
		else {
			this.weights = learnedWeights;
		}
	}
	
	/**
	 * calculates board value
	 * 
	 * @param features features observed on the current board
	 * @return board value
	 */
	public double evaluate(Features features) {
		double sum = 0;
		
		for (int i = 0; i < features.features.length; i++) {
			sum += this.weights[i]*features.features[i];
		}

		return sum;
	}
	
	/**
	 * performs learning based on move history after reaching a final state
	 * 
	 * @param board final state board
	 */
	public void learn(IBoard board) {
		double           target;
		double           targetValue;
		double[]         change;
		FeatureExtractor extractor;
		int              numMoves;
		int              counter;
		IBoard           recreation;
		
		change    = new double[this.weights.length];
		extractor = new FeatureExtractor(this.player);
		
		if (board.getWinner() == this.player) {
			target = 109;
		}
		else if (board.getWinner() != null) {
			target = -109;
		}
		else {
			// draw
			return;
		}
		
		numMoves   = board.getMoveHistory().size();
		recreation = board.clone();
		recreation.clear();
		
		counter = 0;
		for (IMove move : board.getMoveHistory()) {
			if (move.getPlayer() == this.player) {
				targetValue = counter * target / numMoves;
				update(recreation, targetValue, extractor.getFeatures(recreation), change);
			}
			try {
				recreation.makeMove(move);
			} catch (IllegalMoveException e) {
				// should not happen...
				e.printStackTrace();
			}
			counter++;
		}
		update(recreation, target, extractor.getFeatures(recreation), change);
		
		// update weights
		for (int i = 0; i < this.weights.length; i++) {
			this.weights[i] += change[i];
		}
	}
	
	/**
	 * adds new changes to array "change" after evaluating current board and target value
	 * 
	 * @param board       current board
	 * @param targetValue target board value
	 * @param features    features observed on current board 
	 * @param change      array containing how weights should be updated
	 */
	public void update(
			IBoard board, double targetValue, Features features, double[] change) 
	{
		double diff = targetValue - this.evaluate(features);
		for (int i = 0; i < features.features.length; i++) {
			// min(1, x) is used to not overpower small and therefore frequent features
			change[i] += eta * diff * Math.min(1,features.features[i]);
		}
	}
	
	@Override
	public String toString() {
		String s = "";
		for (double w : weights) {
			s += (double) Math.round(w*100)/100;
			s += ", ";
		}
		return s;
	}
}

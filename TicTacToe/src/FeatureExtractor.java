import de.ovgu.dke.teaching.ml.tictactoe.api.IBoard;
import de.ovgu.dke.teaching.ml.tictactoe.api.IMove;
import de.ovgu.dke.teaching.ml.tictactoe.api.IPlayer;
import de.ovgu.dke.teaching.ml.tictactoe.api.IllegalMoveException;

public class FeatureExtractor {
	
	// constants for the different dimensions
	private static final int X = 0;
	private static final int Y = 1;
	private static final int Z = 2;
	
	private IPlayer ownPlayer;
	
	public FeatureExtractor(IPlayer me) {
		this.ownPlayer = me;
	}
	
	public Features getFeatures(IBoard board) {
		Features features;
		
		features = new Features(board);
		
		this.getAxisParallelLines(X, board, features);
		this.getAxisParallelLines(Y, board, features);
		this.getAxisParallelLines(Z, board, features);
		
		this.get2DDiagonals(X, Y, board, features);
		this.get2DDiagonals(X, Z, board, features);
		this.get2DDiagonals(Y, Z, board, features);
		
		this.get3DDiagonals(board, features);
		
		return features;
	}
	
	private void getAxisParallelLines(int axis, IBoard board, Features features) {
		int     ownStones;
		int     oppStones;
		IPlayer player;
		int[]   coordinates;
		
		// lines parallel to given axis
		for (int c = 0; c < board.getSize(); c++) {
			for (int b = 0; b < board.getSize(); b++) {
				ownStones = 0;
				oppStones = 0;
				for (int a = 0; a < board.getSize(); a++) {
					switch (axis) {
					case X:  coordinates = new int[] {a, b, c}; break;
					case Y:  coordinates = new int[] {c, a, b}; break;
					default: coordinates = new int[] {b, c, a};
					}
					
					player = board.getFieldValue(coordinates);
					
					if (player != null) {
						if (player == this.ownPlayer) {
							ownStones++;
						}
						else {
							oppStones++;
						}
					}
				}
				
				features.addLine(ownStones, oppStones);
			}
		}
	}
	
	/**
	 * IMPORTANT: Alphabetically smaller axis first! (Not Y,X)
	 * 
	 * @param axis1
	 * @param axis2
	 * @param board
	 * @param features
	 */
	private void get2DDiagonals(int axis1, int axis2, IBoard board, Features features) {
		int     ownStones;
		int     oppStones;
		IPlayer player;
		int[]   coordinates;
		
		for (int b = 0; b < board.getSize(); b++) {
			ownStones = 0;
			oppStones = 0;
			for (int a = 0; a < board.getSize(); a++) {
				if (axis1 == X) {
					if (axis2 == Y) {
						// X/Y
						coordinates = new int[] {a, a, b};
					}
					else {
						// X/Z
						coordinates = new int[] {a, b, a};
					}
				}
				else {
					// Y/Z
					coordinates = new int[] {b, a, a};
				}
				
				player = board.getFieldValue(coordinates);
				
				if (player != null) {
					if (player == this.ownPlayer) {
						ownStones++;
					}
					else {
						oppStones++;
					}
				}
			}
			
			features.addLine(ownStones, oppStones);
			
			
			ownStones = 0;
			oppStones = 0;
			for (int a = 0; a < board.getSize(); a++) {
				if (axis1 == X) {
					if (axis2 == Y) {
						// X/Y
						coordinates = new int[] {a,board.getSize()-1-a,b};
					}
					else {
						// X/Z
						coordinates = new int[] {board.getSize()-1-a,b,a};
					}
				}
				else {
					// Y/Z
					coordinates = new int[] {b,a,board.getSize()-1-a};
				}
				
				player = board.getFieldValue(coordinates);
				
				if (player != null) {
					if (player == this.ownPlayer) {
						ownStones++;
					}
					else {
						oppStones++;
					}
				}
			}
			
			features.addLine(ownStones, oppStones);
		}
	}
	
	private void get3DDiagonals(IBoard board, Features features) {
		int      ownStones;
		int      oppStones;
		IPlayer  player;
		
		ownStones = 0;
		oppStones = 0;
		for (int x = 0; x < board.getSize(); x++) {
			player = board.getFieldValue(new int[] {x,x,x});
			
			if (player != null) {
				if (player == this.ownPlayer) {
					ownStones++;
				}
				else {
					oppStones++;
				}
			}
		}
		
		features.addLine(ownStones, oppStones);
		
		
		ownStones = 0;
		oppStones = 0;
		for (int x = 0; x < board.getSize(); x++) {
			player = board.getFieldValue(new int[] {x,x,board.getSize()-1-x});
			
			if (player != null) {
				if (player == this.ownPlayer) {
					ownStones++;
				}
				else {
					oppStones++;
				}
			}
		}
		
		features.addLine(ownStones, oppStones);
		
		
		ownStones = 0;
		oppStones = 0;
		for (int x = 0; x < board.getSize(); x++) {
			player = board.getFieldValue(new int[] {x,board.getSize()-1-x,x});
			
			if (player != null) {
				if (player == this.ownPlayer) {
					ownStones++;
				}
				else {
					oppStones++;
				}
			}
		}
		
		features.addLine(ownStones, oppStones);
		
		
		ownStones = 0;
		oppStones = 0;
		for (int x = 0; x < board.getSize(); x++) {
			player = board.getFieldValue(new int[] {board.getSize()-1-x,x,x});
			
			if (player != null) {
				if (player == this.ownPlayer) {
					ownStones++;
				}
				else {
					oppStones++;
				}
			}
		}
		
		features.addLine(ownStones, oppStones);
	}
	
	/**
	 * Determines the difference in features before and after making given move.
	 * Used for incremental calculation of features.
	 * 
	 * @param board board before making given move
	 * @param move
	 * @return difference in features
	 */
	public Features incrementFeatures(IBoard board, IMove move) {
		Features featuresBefore;
		Features featuresAfter;
		Field    field;
		IBoard   copy;
		
		featuresBefore = new Features(board);
		field          = new Field(move.getPosition());
		
		this.getLineThroughField(X, field, board, featuresBefore);
		this.getLineThroughField(Y, field, board, featuresBefore);
		this.getLineThroughField(Z, field, board, featuresBefore);
		
		this.get2DDiagonals(X, Y, board, featuresBefore);
		this.get2DDiagonals(X, Z, board, featuresBefore);
		this.get2DDiagonals(Y, Z, board, featuresBefore);
		
		this.get3DDiagonals(board, featuresBefore);
		
		
		copy = board.clone();
		try {
			copy.makeMove(move);
		} catch (IllegalMoveException e) {
			// should not happen...
			e.printStackTrace();
		}
		
		featuresAfter = new Features(copy);
		
		this.getLineThroughField(X, field, copy, featuresAfter);
		this.getLineThroughField(Y, field, copy, featuresAfter);
		this.getLineThroughField(Z, field, copy, featuresAfter);
		
		this.get2DDiagonals(X, Y, copy, featuresAfter);
		this.get2DDiagonals(X, Z, copy, featuresAfter);
		this.get2DDiagonals(Y, Z, copy, featuresAfter);
		
		this.get3DDiagonals(copy, featuresAfter);
			
		return featuresAfter.diff(featuresBefore);
	}
	
	/**
	 * Counts stones in line parallel to given axis containing given field.
	 * Used for incremental calculation of features.
	 * 
	 * @param axis
	 * @param field
	 * @param board
	 * @param features
	 */
	private void getLineThroughField(int axis, Field field, IBoard board, Features features) {
		int     ownStones;
		int     oppStones;
		IPlayer player;
		int[]   coordinates;
		
		// lines parallel to given axis
		ownStones = 0;
		oppStones = 0;
		for (int a = 0; a < board.getSize(); a++) {
			switch (axis) {
			case X:  coordinates = new int[] {a, field.y, field.z}; break;
			case Y:  coordinates = new int[] {field.x, a, field.z}; break;
			default: coordinates = new int[] {field.x, field.y, a};
			}
			
			player = board.getFieldValue(coordinates);
			
			if (player != null) {
				if (player == this.ownPlayer) {
					ownStones++;
				}
				else {
					oppStones++;
				}
			}
		}
		
		features.addLine(ownStones, oppStones);
	}
}

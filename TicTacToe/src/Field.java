/**
 * Simple field class with coordinates.
 */
public class Field {
	public int x, y, z;
	
	public Field(int _x, int _y, int _z) {
		this.x = _x;
		this.y = _y;
		this.z = _z;
	}
	
	public Field(int[] coordinates) {
		this(coordinates[0], coordinates[1], coordinates[2]);
	}
	
	public int[] getArray() {
		return new int[] {this.x, this.y, this.z};
	}
}

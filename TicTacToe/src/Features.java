import java.util.Arrays;

import de.ovgu.dke.teaching.ml.tictactoe.api.IBoard;

public class Features {
	
	private int   numFeatures;
	public  int[] features; 
	
	public Features(IBoard board) {
		numFeatures = board.getSize() - 2;
		features    = new int[numFeatures * 2];
	}
	
	private Features(int[] _features) {
		this.features = _features;
	}
	
	/**
	 * saves lines containing 3, 4 or 5 stones in features
	 * 
	 * @param ownStones number of own stones in a line
	 * @param oppStones number of opponent's stones in a line
	 */
	public void addLine(int ownStones, int oppStones) {
		if (oppStones == 0) {
			// ignore small features
			if (ownStones >= 3) {
				features[ownStones - 3]++;
			}
		}
		else if (ownStones == 0) {
			// ignore small features
			if (oppStones >= 3) {
				features[numFeatures + oppStones - 3]++;
			}
		}
	}
	
	public Features diff(Features other) {
		int[] diffs;
		
		diffs = new int[this.features.length]; 
		
		for (int i = 0; i < this.features.length; i++) {
			diffs[i] = this.features[i] - other.features[i];
		}
		
		return new Features(diffs);
	}
	
	public Features add(Features other) {
		int[] sums;
		
		sums = new int[this.features.length]; 
		
		for (int i = 0; i < this.features.length; i++) {
			sums[i] = this.features[i] + other.features[i];
		}
		
		return new Features(sums);
	}
	
	public String toString() {
		return Arrays.toString(features);
	}
}
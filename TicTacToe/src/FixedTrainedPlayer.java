/**
 * This player uses a fixed decision strategy, resulting from the earlier conducted
 * learning phase, so it does not learn during the tournament.
 * The weights were obtained in an 8-round-tournament against Random and Smart.
 * 
 * @author Marianne Stecklina, Cornelius Styp von Rekowski
 *
 */
public class FixedTrainedPlayer extends NewPlayer {
	
	public FixedTrainedPlayer() {
		super();
		
		this.function = new Function(this, 
				new double[]{1.09, 19.18, 20.4, -37.67, -74.23, -104.96});
	}
	
	@Override
	public boolean shouldLearn() {
		return false;
	}
}

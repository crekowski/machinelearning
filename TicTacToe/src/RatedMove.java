import de.ovgu.dke.teaching.ml.tictactoe.api.IMove;

public class RatedMove implements Comparable<RatedMove> {

	public IMove  move;
	public double boardValue;
	
	public RatedMove(IMove _move, double _boardValue) {
		this.move       = _move;
		this.boardValue = _boardValue;
	}

	@Override
	public int compareTo(RatedMove m) {
		if (this.boardValue > m.boardValue) {
			return -1;
		}
		if (this.boardValue == m.boardValue) {
			return 0;
		}
		return 1;
	}
}

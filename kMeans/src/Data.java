import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Class for storing all Instances.
 */
public class Data {

	public List<Instance> data;
	public String[]       classValues;
	public String[]       attributeLabels;
	
	/**
	 * Create an empty data set.
	 * 
	 * @param classValues     array of possible class values
	 * @param attributeLabels array of all attributes
	 */
	public Data(
			String[] classValues, 
			String[] attributeLabels) 
	{
		this.data            = new LinkedList<Instance>();
		this.classValues     = classValues;
		this.attributeLabels = attributeLabels;
	}
	
	/**
	 * Choose k random initial centroids from the data set.
	 * 
	 * @param k number of centroids
	 * @return k randomly chosen instances
	 */
	public Instance[] getInitialCentroids(int k) {
		int        rand;
		Instance[] centroids = new Instance[k];
		
		// extract random centroids
		for (int i = 0; i < k; ++i) {
			rand         = (int) (Math.random()*this.data.size());
			centroids[i] = this.data.remove(rand);
		}
		
		// put centroids back into the data set
		this.data.addAll(Arrays.asList(centroids));
		
		return centroids;
	}
	
	/**
	 * Add the given instance to the data set.
	 * 
	 * @param inst
	 */
	public void addInstance(Instance inst) {
		this.data.add(inst);
	}
}

/**
 * Class to represent an instance storing its attribute values and label.
 */
public class Instance {
	
	public int      label;
	public double[] attributes;
	
	/** currently assigned cluster id, -1 if no cluster is assigned */
	public int      cluster;
	
	/**
	 * Create instance with the given attribute values and label.
	 * 
	 * @param label      class label
	 * @param attributes array containing all attribute values
	 */
	public Instance (int label, double[] attributes) {
		this.label      = label;
		this.attributes = attributes;
		this.cluster    = -1;
	}
	
	/**
	 * Distances of this instance to all centroids are calculated and cluster with
	 * minimal distance is assigned.
	 * Also it is returned if the cluster changed.
	 * 
	 * @param centroids
	 * @return true if cluster changed
	 */
	public boolean determineCluster(Instance[] centroids) {
		double minDist    = Double.MAX_VALUE;
		double distance;
		int    oldCluster = this.cluster;
		
		for (int i = 0; i < centroids.length; ++i) {
			distance = 0;
			
			// calculate squared euclidean distance
			for (int attr = 0; attr < this.attributes.length; ++attr) {
				distance += Math.pow(this.attributes[attr] - centroids[i].attributes[attr], 2);
			}
			
			if (distance < minDist) {
				minDist = distance;
				this.cluster = i;
			}
		}
		return this.cluster != oldCluster;
	}
	
}

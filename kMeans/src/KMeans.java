/**
 * Main Class to start the k-Means algorithm.
 * 
 * @author Marianne Stecklina, Cornelius Styp von Rekowski
 */
public class KMeans {
	
	public static void main(String[] args) {
		DataImporter importer = new DataImporter();
		Data         data     = importer.importData();
		int          k        = Integer.parseInt(args[0]);
		
		clustering(data, k);
		
		classification(data, k);
	}
	
	/**
	 * assigns a cluster id to each instance in data
	 * 
	 * @param data training instances
	 * @param k    amount of clusters
	 */
	private static void clustering(Data data, int k) {
		Instance[] centroids;
		boolean    changed;
		int[]      clusterCount;
		double[][] sumAttributes;
		double[]   newCentroid;
		
		centroids = data.getInitialCentroids(k);
		
		do {
			changed       = false;
			clusterCount  = new int[k];
			sumAttributes = new double[k][data.attributeLabels.length];
			for (Instance inst : data.data) {
				// assign new cluster ID and check for changes
				changed |= inst.determineCluster(centroids);
				clusterCount[inst.cluster]++;
				// add up attribute values for centroid calculation
				for (int i = 0; i < data.attributeLabels.length; ++i) {
					sumAttributes[inst.cluster][i] += inst.attributes[i];					
				}
			}
			
			// compute new centroids
			if (changed) {
				for (int i = 0; i < k; ++i) {
					newCentroid = new double[data.attributeLabels.length];
					for (int attr = 0; attr < data.attributeLabels.length; attr++) {
						newCentroid[attr] = sumAttributes[i][attr] / clusterCount[i];
					}
					centroids[i] = new Instance(-1, newCentroid);
				}
			}
		} while (changed);
	}
	
	/**
	 * counts the error rate based on the majority class of each cluster
	 * 
	 * @param data training instances
	 * @param k    amount of clusters
	 */
	private static void classification(Data data, int k) {
		int[][] classCount   = new int[k][data.classValues.length];
		int     maxCount;
		int     n;
		int     correctCount = 0;
		
		// count the amount of instances with a certain label within the clusters
		for (Instance inst : data.data) {
			classCount[inst.cluster][inst.label]++;
		}
		
		for (int i = 0; i < k; ++i) {
			// determine majority class and count correctly classified instances
			maxCount = -1;
			for (int label = 0; label < data.classValues.length; ++label) {
				if (classCount[i][label] > maxCount) {
					maxCount = classCount[i][label];
				}
			}
			correctCount += maxCount;
		}
		
		// print error rate
		n = data.data.size();
		System.out.println("Error rate: " + (n - correctCount) / (double) n);
	}
	
}

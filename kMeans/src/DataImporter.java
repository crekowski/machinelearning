import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to import data from the given files.
 */
public class DataImporter {
	
	private static final String DATA_DIR  = "cardaten/";
	private static final String HEAD_FILE = "car.c45-names";
	private static final String DATA_FILE = "car.data";
	
	private String[]                          classValues;
	private String[]                          attributeLabels;
	private Map<String, Map<String, Integer>> encoding;
	
	/**
	 * Create a DataImporter (initialize encoding).
	 */
	public DataImporter() {
		this.encoding = new HashMap<String, Map<String, Integer>>();
	}
	
	/**
	 * Import all data from the given files. Data configurations are extracted.
	 * 
	 * @return Data set containing all instances.
	 */
	public Data importData() {
		File           headFile;
		File           dataFile;
		BufferedReader reader;
		
		headFile = new File(DATA_DIR + HEAD_FILE);
		dataFile = new File(DATA_DIR + DATA_FILE);
		
		// head file with general configurations
		try {
			reader = new BufferedReader(new FileReader(headFile));
			
			// get class values
			getClassValues(reader);
			
			// get attribute labels
			getAttributeLabels(reader);
					
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			System.err.println("File " + HEAD_FILE + " was not found!");
		}
		
		// create an empty data set with the extracted configurations
		Data data = new Data(classValues, attributeLabels);
		
		// data file with instances
		try {
			reader = new BufferedReader(new FileReader(dataFile));
			
			// get instances
			getInstances(reader, data);
			
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			System.err.println("File " + DATA_FILE + " was not found!");
		}
		
		return data;
	}
	
	/**
	 * Store possible class values in {@link #classValues}.
	 * Integer encoding for class values is created.
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void getClassValues(BufferedReader reader) throws IOException {
		String               line;
		Map<String, Integer> classEncoding;
		
		do {
			// go to class values section
			line = reader.readLine();
		} while (line != null && !line.contains("class values"));
		
		do {
			// go to line with class values
			line = reader.readLine();
		} while (line != null && !line.contains(", "));
		
		this.classValues = line.split(", ");
		
		// create encoding for class values
		classEncoding = new HashMap<String, Integer>();
		
		for (int i = 0; i < this.classValues.length; ++i) {
			classEncoding.put(this.classValues[i], i);
		}
		
		this.encoding.put("class", classEncoding);
	}
	
	/**
	 * Store attributes in {@link #attributeLabels}.
	 * Integer encoding for attribute values is created.
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void getAttributeLabels(BufferedReader reader) throws IOException {
		String               line;
		String               attributes;
		String[]             labelAndAttributes;
		List<String>         attributeLabels;
		String[]             attributeValues;
		Map<String, Integer> attrEncoding;
		
		attributeLabels = new ArrayList<String>();
		
		do {
			// go to attributes section
			line = reader.readLine();
		} while (line != null && !line.contains("attributes"));
		
		do {
			// go to first attribute
			line = reader.readLine();
		} while (line != null && !line.contains(":"));
		
		do {
			// add attribute to list
			labelAndAttributes = line.split(":");
			attributeLabels.add(labelAndAttributes[0]);
			attributes         = labelAndAttributes[1].trim();
			
			// remove trailing "."
			attributes = attributes.replace(".", "");
			
			// create encoding for attribute values
			attrEncoding    = new HashMap<String, Integer>();
			attributeValues = attributes.split(", ");
			
			for (int i = 0; i < attributeValues.length; ++i) {
				attrEncoding.put(attributeValues[i], i);
			}
			
			this.encoding.put(labelAndAttributes[0], attrEncoding);
			
			line = reader.readLine();
		} while (line != null && line.contains(":"));
		
		this.attributeLabels = attributeLabels.toArray(new String[attributeLabels.size()]);
	}
	
	/**
	 * Add all instances to the data set.
	 * Labels and attributes are encoded according to the earlier created encoding.
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void getInstances(BufferedReader reader, Data data) throws IOException {
		String   line;
		String[] attributes;
		double[] encodedAttrs;
		
		line = reader.readLine();
	    while (line != null && !line.isEmpty()) {
	    	attributes   = line.split(",");
	    	encodedAttrs = new double[attributes.length - 1];
	    	
	    	for (int i = 0; i < attributes.length - 1; ++i) {
	    		// get encoded attribute values
	    		encodedAttrs[i] = this.encoding.get(attributeLabels[i]).get(attributes[i]);
	    	}
	    	
	    	data.addInstance(
	    			new Instance(
	    					// use encoded class label
	    					this.encoding.get("class").get(attributes[attributes.length - 1]), 
	    					encodedAttrs));
	    	
			line = reader.readLine();
	    }
	}
}

KMeans is the main class of the project.
When started it runs a k-means clustering (with k=4) on the car data set and calculates 
a classification error rate based on the assumption, that each cluster is
labeled with its majority class.

Our program expects the folder "cardaten" to be inside "kMeans_Cornelius_Marianne".
If this is not the case please change DATA_DIR in class DataImporter to the
new directory.


Results:

Error rate: 0.29976851851851855


Comparison to other classification algorithms:
The kMeans classifier performs much worse than all other considered algorithms.
This is due to the effect that in this approach all instances are labeled as "unacc",
because the classes are unbalanced. One could probably avoid this by using special
weights for the different dimensions.
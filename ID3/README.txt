DecisionTreeBuilder is the main class of the project. 
When started it constructs a decision tree using the ID3 algorithm
on the car data set.
The output is written to a XML file named "decisionTree".

Our program expects the folder "cardaten" to be inside "ID3_Cornelius_Marianne".
If this is not the case please change DATA_DIR in class DataImporter to the
new directory.

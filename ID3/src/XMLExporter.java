import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Class to export the given tree in an XML-file.
 */
public class XMLExporter {
	
	private static final String EXPORT_FILE = "decisionTree.xml";
	
	/**
	 * Export the given tree.
	 * 
	 * @param tree root node of the tree
	 */
	public static void export(TreeNode tree) {
		BufferedWriter writer;
		
		try {
			writer = new BufferedWriter(new FileWriter(new File(EXPORT_FILE)));
			
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
			writer.newLine();
			writeNode(tree, writer, "");
			
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Write contents of the current node to the XML-file,
	 * then repeat the process for the children.
	 * 
	 * @param node
	 * @param writer
	 * @param indent
	 * @throws IOException
	 */
	private static void writeNode(TreeNode node, BufferedWriter writer, String indent) 
			throws IOException 
	{
		String numInstances;
		
		// start tag
		if (node.splitAttr == null) {
			// root
			writer.write("<tree classes=\"");
		}
		else {
			writer.write(indent + "<node classes=\"");
		}
		
		// get class distribution
		numInstances = "";
		for (String label : node.data.data.keySet()) {
			numInstances += label + ":" + node.data.data.get(label).size() + ",";
		}
		
		// remove last "," and add rounded entropy
		writer.write(numInstances.substring(0, numInstances.length() - 1) 
				     + "\" entropy=\"" + Math.round(node.entropy*1000)/1000d + "\"");
		
		// get split attribute and value
		if (node.splitAttr != null) {
			writer.write(" " + node.splitAttr + "=\"" + node.splitValue + "\"");
		}
		
		writer.write(">");
		
		if (node.children != null) {
			// write contents for the children
			writer.newLine();
			for (TreeNode child : node.children) {
				writeNode(child, writer, indent + "  ");
			}
		}
		else {
			// leaf
			writer.write(node.label);
			indent = "";
		}
		
		// end tag
		if (node.splitAttr == null) {
			// root
			writer.write("</tree>");
		}
		else {
			writer.write(indent + "</node>");
			writer.newLine();
		}
	}
}

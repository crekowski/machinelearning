import java.util.Arrays;

/**
 * Main Class to start the ID3 algorithm.
 * 
 * @author Marianne Stecklina, Cornelius Styp von Rekowski
 */
public class DecisionTreeBuilder {
	
	public static void main(String[] args) {
		DataImporter importer;
		TreeNode     root;
		Data         data;
		
		importer = new DataImporter();
		
		data = importer.importData();
		root = new TreeNode(data, null, null, Arrays.asList(data.attributeLabels));
		
		XMLExporter.export(root);
	}
}

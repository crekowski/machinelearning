import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class to store and organize all instances at a node.
 */
public class Data {

	public String[]                   classValues;
	public String[]                   attributeLabels;
	public Map<String, Set<String>>   attributeValues;
	public Map<String, Set<Instance>> data;
	public int                        numInstances;
	public Map<String, Integer>       numInstancesPerClass;
	
	/**
	 * Create an empty Data set with the given configurations.
	 * 
	 * @param classValues     array of possible class values
	 * @param attributeLabels array of all attributes
	 * @param attributeValues map with possible values for each attribute
	 */
	public Data(
			String[] classValues, 
			String[] attributeLabels, 
			Map<String, Set<String>> attributeValues) 
	{
		this.classValues          = classValues;
		this.attributeLabels      = attributeLabels;
		this.attributeValues      = attributeValues;
		this.numInstances         = 0;
		this.numInstancesPerClass = new HashMap<String, Integer>();
		this.data                 = new HashMap<String, Set<Instance>>();
		
		for (String label : this.classValues) {
			this.numInstancesPerClass.put(label, 0);
			this.data.put(label, new HashSet<Instance>());
		}
	}
	
	/**
	 * Add the given instance to the data set.
	 * The internal counters are raised accordingly.
	 * 
	 * @param inst
	 */
	public void addInstance(Instance inst) {
		this.data.get(inst.label).add(inst);
		
		this.numInstances++;
		this.numInstancesPerClass.put(inst.label, 
				this.numInstancesPerClass.get(inst.label) + 1);
	}
	
	/**
	 * Get all instances for the given class.
	 * 
	 * @param label class
	 * @return set of all instances for the class
	 */
	public Set<Instance> getDataforClass(String label) {
		return this.data.get(label);
	}
	
	/**
	 * Split the data for the given attribute (partition according to attribute values).
	 * 
	 * @param attribute to split
	 * @return a map with a data entry for each possible attribute value
	 */
	public Map<String, Data> splitDataForAttribute(String attribute) {
		Map<String, Data> newData;
		int               attrLabelIndex;
		
		newData        = new HashMap<String, Data>();
		attrLabelIndex = this.getIndexOfAttrLabel(attribute);
		
		for (String attrValue : this.attributeValues.get(attribute)) {
			newData.put(attrValue, 
					new Data(
							this.classValues, 
							this.attributeLabels, 
							this.attributeValues));
		}
		
		for (String label : data.keySet()) {
			for (Instance inst : data.get(label)) {
				newData.get(inst.getValueForAttrIndex(attrLabelIndex)).addInstance(inst);
			}
		}
		
		return newData;
	}
	
	/**
	 * Get index of the given attribute in the attribute array.
	 * 
	 * @param label attribute
	 * @return index of the attribute
	 */
	private int getIndexOfAttrLabel(String label) {
		for (int i = 0; i < this.attributeLabels.length; i++) {
			if (this.attributeLabels[i].equals(label)) {
				return i;
			}
		}
		
		return -1;
	}
}

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class TreeNode {
	
	public  List<TreeNode>    children;
	public  Data              data;
	public  String            splitAttr;
	public  String            splitValue;
	public  String            label;
	public  double            entropy;
	private List<String>      openAttrs;
	private Map<String, Data> lastSplit;
	
	/**
	 * Create new node and split it.
	 * 
	 * @param data       all instances at this node
	 * @param splitAttr  split attribute at the parent node
	 * @param splitValue value of the split attribute for the instances at this node
	 * @param openAttrs  list of attributes not yet used as split attributes
	 */
	public TreeNode(
			Data         data, 
			String       splitAttr, 
			String       splitValue, 
			List<String> openAttrs) 
	{
		this.data       = data;
		this.splitAttr  = splitAttr;
		this.splitValue = splitValue;
		this.label      = null;
		this.openAttrs  = openAttrs;
		
		this.split();
	}
	
	/**
	 * Constructor for nodes with empty data sets.
	 * -> take major label from parent
	 * 
	 * @param splitAttr  split attribute at the parent node
	 * @param splitValue value of the split attribute for the instances at this node
	 * @param label      major label at parent node
	 */
	public TreeNode(String splitAttr, String splitValue, String label) {
		this.splitAttr  = splitAttr;
		this.splitValue = splitValue;
		this.label      = label;
		this.data       = null;
		this.children   = null;
		this.openAttrs  = null;
		this.entropy    = 0;
	}
	
	/**
	 *  Split node until it is pure, the data set is empty or there are no split attributes left.
	 */
	private void split() {
		Map<String, Data> bestSplit;
		String            bestSplitAttr;
		double            bestSplitEntropy;
		double            currentSplitEntropy;
		List<String>      childrenOpenAttributes;
		
		bestSplit     = null;
		bestSplitAttr = null;
		
		if (this.openAttrs.isEmpty()) {
			this.label = this.getMajorLabel();
		}
		else {
			this.entropy     = this.entropy(this.data);
			// pure node --> leaf
			if (this.entropy == 0) {
				// label has already been set in function entropy
			}
			else {
				bestSplitEntropy = 2;
				
				// find split attribute with lowest entropy
				for (String attr : this.openAttrs) {
					currentSplitEntropy = this.getEntropyForSplit(attr, data.numInstances);
					
					if (currentSplitEntropy < bestSplitEntropy) {
						bestSplitEntropy = currentSplitEntropy;
						bestSplitAttr    = attr;
						bestSplit        = this.lastSplit;
					}
				}
				
				children = new ArrayList<TreeNode>(
						this.data.attributeValues.get(bestSplitAttr).size());
				
				childrenOpenAttributes = new ArrayList<String>(openAttrs);
				childrenOpenAttributes.remove(bestSplitAttr);
				
				// add children for each possible attribute value
				for (String attrValue : bestSplit.keySet()) {
					if (bestSplit.get(attrValue).numInstances == 0) {
						// empty leaf -> take main class from parent
						children.add(new TreeNode(
								bestSplitAttr, 
								attrValue, 
								this.getMajorLabel()));
					}
					else {
						children.add(new TreeNode(
								bestSplit.get(attrValue), 
								bestSplitAttr, 
								attrValue, 
								childrenOpenAttributes));
					}
				}
			}
		}
	}
	
	/**
	 * Determine major label among all instances at this node.
	 * 
	 * @return major label at this node
	 */
	public String getMajorLabel() {
		String majorLabel;
		int    highestN;
		int    currentN;
		
		majorLabel = null;
		highestN  = 0;
		
		for (String label : this.data.classValues) {
			currentN = this.data.numInstancesPerClass.get(label);
			if (currentN > highestN) {
				highestN  = currentN;
				majorLabel = label;
			}
		}
		
		return majorLabel;
	}
	
	/**
	 * Calculate entropy after split by the given attribute.
	 * 
	 * @param attr         split attribute
	 * @param numInstances number of instances at parent node
	 * @return entropy after split
	 */
	public double getEntropyForSplit(String attr, int numInstances) {
		double entropy;
		Data   data;
		
		entropy        = 0;
		this.lastSplit = this.data.splitDataForAttribute(attr);
			
		for (String value : this.lastSplit.keySet()) {
			data = this.lastSplit.get(value);
			entropy += data.numInstances / (double) numInstances * entropy(data);
		}
		
		return entropy;
	}
	
	/**
	 * Calculate entropy for this node.
	 * 
	 * @param data all instances at this node
	 * @return entropy
	 */
	public double entropy(Data data) {
		Map<String, Integer> numInstances;
		double               ratio;
		double               entropy;
		
		entropy      = 0;
		numInstances = data.numInstancesPerClass;
		
		for (String label : numInstances.keySet()) {
			ratio = numInstances.get(label) / (double) data.numInstances;
			if (ratio != 0) {
				entropy += ratio * Math.log(ratio) / Math.log(data.classValues.length);
			}
			// leaf node --> assign label
			if (ratio == 1) {
				this.label = label;
			}
		}
		
		return -1 * entropy;
	}
}

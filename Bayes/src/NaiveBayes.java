import java.util.HashMap;
import java.util.Map;

/**
 * Main Class to start the Naive Bayes algorithm.
 * 
 * @author Marianne Stecklina, Cornelius Styp von Rekowski
 */
public class NaiveBayes {
	
	public static void main(String[] args) {
		DataImporter         importer        = new DataImporter();
		Data                 data            = importer.importData();
		String               label;
		int                  errors          = 0;
		int                  num             = 0;
		Map<String, Integer> confusionMatrix = new HashMap<String, Integer>();
		
		// initialize confusion matrix
		for (String trueLabel : data.classValues) {
			for (String bayesLabel : data.classValues) {
				confusionMatrix.put(trueLabel + bayesLabel, 0);				
			}
		}
		
		// 100 random partitions
		for (int i = 0; i < 100; ++i) {
			data.partition();
			for (Instance inst : data.testData) {
				++num;
				label = classify(inst, data);
				// count errors / update confusion matrix
				if (!label.equals(inst.label)) {
					++errors;
				}
				if (i == 0) {
					confusionMatrix.put(inst.label + label, confusionMatrix.get(inst.label + label) + 1);
				}
			}
		}
		System.out.println("Error rate: " + errors / (double) num);
		
		plotConfusionMatrix(confusionMatrix, data);
	}
	
	/**
	 * Classifies an instance using Naive Bayes
	 * 
	 * @param  inst instance to classify
	 * @param  data data set split into training and test set  
	 * @return class name
	 */
	private static String classify(Instance inst, Data data) {
		double maxProb  = -1;
		double prob     = 0;
		String optClass = null;
		
		for (String label : data.classValues) {
			// class probability
			prob = data.classProbabilities.get(label);
			
			// conditional probabilities for given attribute values
			for (int attr = 0; attr < data.attributeLabels.length; ++attr) {
				prob *= data.attributeProbabilities.get(label).get(data.attributeLabels[attr]).get(inst.attributes[attr]);
			}
			
			// find class with highest probability
			if (prob > maxProb) {
				maxProb  = prob;
				optClass = label;
			}
		}

		return optClass;
	}
	
	/**
	 * Plots a confusion matrix to the console.
	 * 
	 * @param confMat Hashmap with key trueLabel+bayesLabel containing the 
	 *                amount of instances from class trueLabel classified as bayesLabel 
	 * @param data    data set containing information about the classValues
	 */
	public static void plotConfusionMatrix(Map<String, Integer> confMat, Data data) {
		// first line
		System.out.print("\nTrue \\ Bayes ");
		for (String label : data.classValues) {
			System.out.print("| " + label);
			for (int i = 0; i < 6 - label.length(); ++i) {
				System.out.print(" ");
			}
		}
		
		int num;
		for (String trueLabel : data.classValues) {
			// separating line
			System.out.print("\n-------------");
			for (int i = 0; i < data.classValues.length; ++i) {
				System.out.print("|-------");
			}
					
			// true label
			System.out.print("\n" + trueLabel);
			for (int i = 0; i < 13 - trueLabel.length(); ++i) {
				System.out.print(" ");
			}
			
			// confusion matrix values
			for (String bayesLabel : data.classValues) {
				num = confMat.get(trueLabel + bayesLabel);
				System.out.print("| " + num);
				for (int i = 0; i < 6 - String.valueOf(num).length(); ++i) {
					System.out.print(" ");
				}
				
			}
		}
 	}

}

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class for storing all Instances. 
 * Partitioning into training and test set can be performed, while the relevant
 * probabilities are recalculated.
 */
public class Data {

	public  List<Instance>                                testData;
	public  List<Instance>                                trainingData;
	private Map<String, Integer>                          classTemplate;
	public  Map<String, Integer>                          classProbabilities;
	private Map<String, Map<String, Map<String, Double>>> attributeTemplate;
	public  Map<String, Map<String, Map<String, Double>>> attributeProbabilities;
	public  String[]                                      classValues;
	public  String[]                                      attributeLabels;
	private Map<String, Set<String>>                      attributeValues;
	
	/**
	 * Create an empty data set. Based on the given configurations
	 * templates for saving probabilities later are constructed.
	 * 
	 * @param classValues     array of possible class values
	 * @param attributeLabels array of all attributes
	 * @param attributeValues map with possible values for each attribute
	 */
	public Data(
			String[]                 classValues, 
			String[]                 attributeLabels, 
			Map<String, Set<String>> attributeValues) 
	{
		this.testData              = new LinkedList<Instance>();
		this.trainingData      = new LinkedList<Instance>();
		this.classTemplate     = new HashMap<String, Integer>();
		this.attributeTemplate = new HashMap<String, Map<String, Map<String, Double>>>();
		this.classValues       = classValues;
		this.attributeLabels   = attributeLabels;
		this.attributeValues   = attributeValues;
		
		// create probability templates (initialize values with 0)
		for (String label : classValues) {
			// class probabilities
			this.classTemplate.put(label, 0);
			
			// attribute probabilities given this class
			this.attributeTemplate.put(label, new HashMap<String, Map<String, Double>>());
			for (String attributeLabel : attributeLabels) {
				this.attributeTemplate.get(label).put(attributeLabel, new HashMap<String, Double>());
				
				// add an entry for each possible value of each attribute
				for (String attributeValue : attributeValues.get(attributeLabel)) {
					this.attributeTemplate.get(label).get(attributeLabel).put(attributeValue, 0d);
				}
			}
		}
	}
	
	/**
	 * Add the given instance to the data set.
	 * 
	 * @param inst
	 */
	public void addInstance(Instance inst) {
		this.testData.add(inst);
	}
	
	/**
	 * Split the data into training and test set. Calculate class and attribute probabilities.
	 */
	public void partition() {
		// reset partitioning from the last trial
		this.testData.addAll(this.trainingData);
		this.trainingData = new LinkedList<Instance>();
		
		// initialize probabilities with 0 (from templates)
		this.classProbabilities     = new HashMap<String, Integer>(this.classTemplate);
		this.attributeProbabilities = new HashMap<String, Map<String, Map<String, Double>>>(this.attributeTemplate);
		
		int      n = this.testData.size();
		int      rand;
		double   count;
		Instance inst;
		
		// randomly extract training set (taking 2/3 of all available instances)
		for (int i = 0; i < (n*2)/3; ++i) {
			rand = (int) (Math.random()*this.testData.size());
			inst = this.testData.remove(rand);
			this.trainingData.add(inst);
			
			// count occurrences of classes
			this.classProbabilities.put(inst.label, this.classProbabilities.get(inst.label) + 1);
			
			// count occurrences of attribute values
			for (int attr = 0; attr < this.attributeLabels.length; ++attr) {
				count = this.attributeProbabilities.get(inst.label).get(this.attributeLabels[attr]).get(inst.attributes[attr]);
				this.attributeProbabilities.get(inst.label).get(this.attributeLabels[attr]).put(inst.attributes[attr], count + 1);
			}
		}
		
		// divide attribute counts by class counts to get probabilities
		for (String label : this.classValues) {
			for (String attr : this.attributeLabels) {
				for (String attrValue : this.attributeValues.get(attr)) {
					if (this.classProbabilities.get(label) == 0) {
						// avoid division by 0
						this.attributeProbabilities.get(label).get(attr).put(attrValue, 0d);
					}
					else {
						count = this.attributeProbabilities.get(label).get(attr).get(attrValue);
						this.attributeProbabilities.get(label).get(attr).put(attrValue, count / this.classProbabilities.get(label));						
					}
				}
			}
		}
		
		// REMARK: class counts do not need to be divided by n, because the relation stays the same
	}
}

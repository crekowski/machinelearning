/**
 * Class to represent an instance storing its attribute values and label.
 */
public class Instance {
	
	public String   label;
	public String[] attributes;
	
	/**
	 * Create instance with the given attribute values and label.
	 * 
	 * @param attributesAndClass array containing all attribute values and as last element the class
	 */
	public Instance (String[] attributesAndClass) {
		this.label      = attributesAndClass[attributesAndClass.length - 1];
		this.attributes = new String[attributesAndClass.length - 1];
		
		for (int i = 0; i < attributesAndClass.length - 1; i++) {
			this.attributes[i] = attributesAndClass[i];
		}
	}
	
}

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class to import data from the given files.
 */
public class DataImporter {
	
	private static final String DATA_DIR  = "cardaten/";
	private static final String HEAD_FILE = "car.c45-names";
	private static final String DATA_FILE = "car.data";
	
	private String[]                 classValues;
	private String[]                 attributeLabels;
	private Map<String, Set<String>> attributeValues;
	
	/**
	 * Import all data from the given files. Data configurations are extracted.
	 * 
	 * @return Data set containing all instances.
	 */
	public Data importData() {
		File           headFile;
		File           dataFile;
		BufferedReader reader;
		
		headFile = new File(DATA_DIR + HEAD_FILE);
		dataFile = new File(DATA_DIR + DATA_FILE);
		
		// head file with general configurations
		try {
			reader = new BufferedReader(new FileReader(headFile));
			
			// get class values
			getClassValues(reader);
			
			// get attribute labels and values
			getAttributeLabelsAndValues(reader);
					
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} catch (IOException e) {
			System.err.println("File " + HEAD_FILE + " was not found!");
		}
		
		// create empty data set with the extracted configurations
		Data data = new Data(classValues, attributeLabels, attributeValues);
		
		// data file with instances
		try {
			reader = new BufferedReader(new FileReader(dataFile));
			
			// get instances
			getInstances(reader, data);
			
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			System.err.println("File " + DATA_FILE + " was not found!");
		}
		
		return data;
	}
	
	/**
	 * Store possible class values in {@link #classValues}.
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void getClassValues(BufferedReader reader) throws IOException {
		String line;
		
		do {
			// go to class values section
			line = reader.readLine();
		} while (line != null && !line.contains("class values"));
		
		do {
			// go to line with class values
			line = reader.readLine();
		} while (line != null && !line.contains(", "));
		
		this.classValues = line.split(", ");
	}
	
	/**
	 * Store attributes and their possible values in {@link #attributeLabels} and {@link #attributeValues}.
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void getAttributeLabelsAndValues(BufferedReader reader) throws IOException {
		String                   line;
		String                   attributes;
		String[]                 labelAndAttributes;
		List<String>             attributeLabels;
		Map<String, Set<String>> attributeValues;
		
		attributeLabels = new ArrayList<String>();
		attributeValues = new HashMap<String, Set<String>>();
		
		do {
			// go to attributes section
			line = reader.readLine();
		} while (line != null && !line.contains("attributes"));
		
		do {
			// go to first attribute
			line = reader.readLine();
		} while (line != null && !line.contains(":"));
		
		do {
			// add attribute to list
			labelAndAttributes = line.split(":");
			attributeLabels.add(labelAndAttributes[0]);
			attributes         = labelAndAttributes[1].trim();
			
			// remove trailing "."
			attributes = attributes.replace(".", "");
			
			attributeValues.put(labelAndAttributes[0], 
					new HashSet<String>(Arrays.asList(attributes.split(", "))));
			
			line = reader.readLine();
		} while (line != null && line.contains(":"));
		
		this.attributeLabels = attributeLabels.toArray(new String[attributeLabels.size()]);
		this.attributeValues = attributeValues;
	}
	
	/**
	 * Add all instances to the data set.
	 * 
	 * @param reader
	 * @throws IOException
	 */
	public void getInstances(BufferedReader reader, Data data) throws IOException {
		String line;
		
		line = reader.readLine();
	    while (line != null && !line.isEmpty()) {
	    	data.addInstance(new Instance(line.split(",")));
	    	
			line = reader.readLine();
	    }
	}
}

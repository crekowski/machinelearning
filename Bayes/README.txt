NaiveBayes is the main class of the project. 
When started it randomly partitions the car data set into training and test set, 
plots a confusion matrix for the first partition and determines the 
classification error over 100 partitions.  

Our program expects the folder "cardaten" to be inside "NaiveBayes_Cornelius_Marianne".
If this is not the case please change DATA_DIR in class DataImporter to the
new directory.


Results:

Error rate: 0.13996527777777779


True \ Bayes | unacc | acc   | good  | vgood 
-------------|-------|-------|-------|-------
unacc        | 388   | 30    | 1     | 0     
-------------|-------|-------|-------|-------
acc          | 10    | 98    | 5     | 0     
-------------|-------|-------|-------|-------
good         | 0     | 12    | 9     | 0     
-------------|-------|-------|-------|-------
vgood        | 0     | 15    | 0     | 8  

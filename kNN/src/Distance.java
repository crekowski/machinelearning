/**
 * Class to store the distance of a known instance to a requested object.
 */
public class Distance implements Comparable<Distance> {

	public Instance inst;
	public double   distance;
	
	/**
	 * Calculate Distance of a known instance to the requested object.
	 * 
	 * @param ownInst       known instance
	 * @param requestedInst requested object
	 */
	public Distance(Instance ownInst, Instance requestedInst) {
		this.inst     = ownInst;
		this.distance = 0;
		
		// calculate squared euclidean distance
		for (int i = 0; i < ownInst.attributes.length; ++i) {
			this.distance += Math.pow(ownInst.attributes[i] - requestedInst.attributes[i], 2);
		}		
	}

	/**
	 * Compare the own {@link #distance} to the given distance. 
	 */
	@Override
	public int compareTo(Distance o) {
		return (int) (Math.signum(this.distance - o.distance));
	}
	
}

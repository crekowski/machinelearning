/**
 * Class to represent an instance storing its attribute values and label.
 */
public class Instance {
	
	public String label;
	public int[]  attributes;
	
	/**
	 * Create instance with the given attribute values and label.
	 * 
	 * @param label      class label
	 * @param attributes array containing all attribute values
	 */
	public Instance (String label, int[] attributes) {
		this.label      = label;
		this.attributes = attributes;
	}
}

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

/**
 * Main Class to start the k-NN algorithm.
 * 
 * @author Marianne Stecklina, Cornelius Styp von Rekowski
 */
public class KNN {
	
	public static void main(String[] args) {
		DataImporter         importer        = new DataImporter();
		Data                 data            = importer.importData();
		String               label;
		int                  errors          = 0;
		int                  num             = 0;
		Map<String, Integer> confusionMatrix = new HashMap<String, Integer>();
		int                  k               = Integer.parseInt(args[0]);
		
		// initialize confusion matrix
		for (String classLabel : data.classValues) {
			for (String classLabel2 : data.classValues) {
				confusionMatrix.put(classLabel + classLabel2, 0);				
			}
		}
		
		// 100 random partitions
		for (int i = 0; i < 100; ++i) {
			data.partition();
			for (Instance inst : data.testData) {
				++num;
				label = classify(inst, data, k);
				// count errors / update confusion matrix
				if (!label.equals(inst.label)) {
					++errors;
				}
				if (i == 0) {
					confusionMatrix.put(inst.label + label, confusionMatrix.get(inst.label + label) + 1);
				}
			}
		}
		System.out.println("Error rate: " + errors / (double) num);
		
		plotConfusionMatrix(confusionMatrix, data);
	}
	
	/**
	 * Classifies an instance considering the k nearest neighbors
	 * 
	 * @param  inst instance to classify
	 * @param  data data set split into training and test set  
	 * @param  k    number of neighbors to determine class
	 * @return class name 
	 */
	private static String classify(Instance inst, Data data, int k) {
		int                     maxCnt   = -1;
		String                  optClass = null;
		PriorityQueue<Distance> queue    = new PriorityQueue<Distance>();
		Distance                dist;
		Map<String, Integer>    cnts     = new HashMap<String, Integer>();
		String                  label;
		
		// build a priority queue ordered by distance
		for (Instance trainingInst : data.trainingData) {
			dist = new Distance(trainingInst, inst);
			queue.add(dist);
		}
		
		for (String classValue : data.classValues) {
			cnts.put(classValue, 0);
		}
		
		// get the k nearest neighbors and increment counts for their label in cnts
		for (int i = 0; i < k; ++i) {
			label = queue.poll().inst.label;
			cnts.put(label, cnts.get(label) + 1);
		}
		
		// determine majority class
		for (String classValue : data.classValues) {
			if (cnts.get(classValue) > maxCnt) {
				optClass = classValue;
				maxCnt   = cnts.get(classValue);
			}
		}
		
		return optClass;
	}
	
	/**
	 * Plots a confusion matrix to the console.
	 * 
	 * @param confMat Hashmap with key trueLabel+kNNLabel containing the 
	 *                amount of instances from class trueLabel classified as kNNLabel 
	 * @param data    data set containing information about the classValues
	 */
	public static void plotConfusionMatrix(Map<String, Integer> confMat, Data data) {
		// first line
		System.out.print("\nTrue \\ kNN   ");
		for (String label : data.classValues) {
			System.out.print("| " + label);
			for (int i = 0; i < 6 - label.length(); ++i) {
				System.out.print(" ");
			}
		}
		
		int num;
		for (String trueLabel : data.classValues) {
			// separating line
			System.out.print("\n-------------");
			for (int i = 0; i < data.classValues.length; ++i) {
				System.out.print("|-------");
			}
					
			// true label
			System.out.print("\n" + trueLabel);
			for (int i = 0; i < 13 - trueLabel.length(); ++i) {
				System.out.print(" ");
			}
			
			// confusion matrix values
			for (String kNNLabel : data.classValues) {
				num = confMat.get(trueLabel + kNNLabel);
				System.out.print("| " + num);
				for (int i = 0; i < 6 - String.valueOf(num).length(); ++i) {
					System.out.print(" ");
				}
				
			}
		}
	}
}

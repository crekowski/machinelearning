import java.util.LinkedList;
import java.util.List;

/**
 * Class for storing all Instances. 
 * Partitioning into training and test set can be performed.
 */
public class Data {

	public  List<Instance> testData;
	public  List<Instance> trainingData;
	public  String[]       classValues;
	public  String[]       attributeLabels;
	
	/**
	 * Create an empty data set.
	 * 
	 * @param classValues     array of possible class values
	 * @param attributeLabels array of all attributes
	 */
	public Data(
			String[] classValues, 
			String[] attributeLabels) 
	{
		this.testData        = new LinkedList<Instance>();
		this.trainingData    = new LinkedList<Instance>();
		this.classValues     = classValues;
		this.attributeLabels = attributeLabels;
	}
	
	/**
	 * Add the given instance to the data set.
	 * 
	 * @param inst
	 */
	public void addInstance(Instance inst) {
		this.testData.add(inst);
	}
	
	/**
	 * Split the data into training and test set.
	 */
	public void partition() {
		// reset partitioning from the last trial
		this.testData.addAll(this.trainingData);
		this.trainingData = new LinkedList<Instance>();
		
		int n = this.testData.size();
		int rand;
		
		// randomly extract training set (taking 2/3 of all available instances)
		for (int i = 0; i < (n*2)/3; ++i) {
			rand = (int) (Math.random()*this.testData.size());
			this.trainingData.add(this.testData.remove(rand));
		}
	}
}

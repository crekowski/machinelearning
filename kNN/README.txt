KNN is the main class of the project. 
When started it randomly partitions the car data set into training and test set, 
plots a confusion matrix for the first partition and determines the 
classification error over 100 partitions.  

Our program expects the folder "cardaten" to be inside "KNN_Cornelius_Marianne".
If this is not the case please change DATA_DIR in class DataImporter to the
new directory.


Results:

k=1:

Error rate: 0.16416666666666666

True \ kNN   | unacc | acc   | good  | vgood 
-------------|-------|-------|-------|-------
unacc        | 355   | 26    | 5     | 0     
-------------|-------|-------|-------|-------
acc          | 43    | 96    | 7     | 5     
-------------|-------|-------|-------|-------
good         | 4     | 3     | 11    | 2     
-------------|-------|-------|-------|-------
vgood        | 0     | 1     | 5     | 13    


k=5:

Error rate: 0.06520833333333333

True \ kNN   | unacc | acc   | good  | vgood 
-------------|-------|-------|-------|-------
unacc        | 389   | 6     | 0     | 0     
-------------|-------|-------|-------|-------
acc          | 21    | 106   | 0     | 0     
-------------|-------|-------|-------|-------
good         | 1     | 17    | 10    | 0     
-------------|-------|-------|-------|-------
vgood        | 0     | 4     | 3     | 19    


Comparison to Naive Bayes:
The kNN classifier performs much better with k=5 in contrast to k=1.
With the bigger k it also produces half the error rate of the Bayes classifier.